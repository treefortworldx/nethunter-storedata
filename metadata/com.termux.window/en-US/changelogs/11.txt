* Fix Android 8 compatibility.
* Update the terminal emulation to match the main app.
* Support custom fonts.
